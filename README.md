* Allumer le mode XDebug dans PHPStorm
* Vérifier depuis l'hôte que le socket s'ouvre bien: telnet host.docker.internal 9003
* Builder le conteneur : docker build . -t test-xdebug
* Créer le conteneur puis ouvrir une session : docker run -it --rm --add-host=host.docker.internal:host-gateway test-xdebug bash
* Vérifier la connection dans le conteneur : telnet host.docker.internal 9003
